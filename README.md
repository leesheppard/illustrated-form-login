[![GitHub version](https://badge.fury.io/gh/leesheppard%2Fillustrated-form-login.svg)](https://badge.fury.io/gh/leesheppard%2Fillustrated-form-login)

# illustrated-form-login
This form was completed as a proof of concept. Many forms are simple in design with the aim to have them completed as quickly and load as efficiently as possible. This is to show you can entertain as well as provide a visual guide to ESL (english as a second language) users and still deliver efficiently to a browser window.

## Contact
Thanks for checking out my work.

![Lee Sheppard signature](http://res.cloudinary.com/leesheppard/image/upload/v1496495524/Lee-Sheppard-Black_iv1j84.png)

[![Hello](https://img.shields.io/badge/Hello-%40leesheppard-blue.svg)](https://twitter.com/leesheppard)

------

![Lee's AntiPirate](http://res.cloudinary.com/leesheppard/image/upload/c_scale,h_147/v1496056672/leesheppard_pirate_jk4fta.png)

## Copyright notice

Illustrations and images used within this project are subject to copyright &copy; 2018 by [Lee Sheppard](http://www.leesheppard.com). All rights are reserved.

### MIT License

Copyright (c) 2018 Lee Sheppard

Permission is hereby granted, free of charge, to any person obtaining a copy
of this SOFTWARE ONLY and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

