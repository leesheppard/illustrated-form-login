$(function( ) {
    var toggleImage = function(className) {
        $('#image').toggleClass(className);
    }
    var toggleUsernameImage =
            toggleImage.bind(null, 'login-username');
    var togglePasswordImage =
            toggleImage.bind(null, 'login-password');

    $('#username').focus(toggleUsernameImage);
    $('#username').blur(toggleUsernameImage);
    $('#password').focus(togglePasswordImage);
    $('#password').blur(togglePasswordImage);
})

$('#forget-password').click(function () {
    $('#loginform, #forgot-link').fadeOut(function () {
			$('#image').addClass('login-forgotten');
      $('#forgotform').fadeIn(200);
    });
  });
  